#//////////////////////////////////////////////////////////////////////////////////////
#	Base make-variables: compiler, compile options, source-flag, sources directory,
#	bins directory and etc.
#//////////////////////////////////////////////////////////////////////////////////////
CC = g++
CFLAGS = -Wall -Wextra
SOURCES = -c
SRC_DIR = src
BIN_DIR = bin
LIB_DIR = lib
OBJ_DIR = obj

#//////////////////////////////////////////////////////////////////////////////////////
#	Project sub-directories: generator folder,
#	math module folder, artist etc.
#//////////////////////////////////////////////////////////////////////////////////////
GENERATOR_DIR = generator
STATISTIC_DIR = statistic
PLOTTER_DIR = plotter
DISTRIBUTIONS_DIR = distributions

#//////////////////////////////////////////////////////////////////////////////////////
#	Distributions
#	Variables
#//////////////////////////////////////////////////////////////////////////////////////
DISTRIBUTION_LIB = libdistribution.a

_DISTRIBUTION_OBJ = normal_distribution.o uniform_distribution.o exponential_distribution.o simple_distribution.o
DISTRIBUTION_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISTRIBUTIONS_DIR)/%, $(_DISTRIBUTION_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	Main
#	Variables
#//////////////////////////////////////////////////////////////////////////////////////
GENERATOR_MAIN = seqgen
STATISTIC_MAIN = seqstat
PLOTTER_MAIN = hplot

_GENERATOR_OBJ = main.o
GENERATOR_OBJ = $(patsubst %, $(OBJ_DIR)/$(GENERATOR_DIR)/%, $(_GENERATOR_OBJ))

_STATISTIC_OBJ = main.o stat_unit.o
STATISTIC_OBJ = $(patsubst %, $(OBJ_DIR)/$(STATISTIC_DIR)/%, $(_STATISTIC_OBJ))

_PLOTTER_OBJ = main.o geometry.o types.o hplotter.o
PLOTTER_OBJ = $(patsubst %, $(OBJ_DIR)/$(PLOTTER_DIR)/%, $(_PLOTTER_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	Distributions
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/$(DISTRIBUTIONS_DIR)/%.o: $(SRC_DIR)/$(DISTRIBUTIONS_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(LIB_DIR)/$(DISTRIBUTION_LIB): $(DISTRIBUTION_OBJ)
	ar rvs $@ $^

#//////////////////////////////////////////////////////////////////////////////////////
#	Generator
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/$(GENERATOR_DIR)/%.o: $(SRC_DIR)/$(GENERATOR_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(GENERATOR_MAIN): $(GENERATOR_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lboost_program_options

#//////////////////////////////////////////////////////////////////////////////////////
#	Statistic
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/$(STATISTIC_DIR)/%.o: $(SRC_DIR)/$(STATISTIC_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(STATISTIC_MAIN): $(STATISTIC_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lboost_program_options

#//////////////////////////////////////////////////////////////////////////////////////
#	Plotter
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/$(PLOTTER_DIR)/%.o: $(SRC_DIR)/$(PLOTTER_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(PLOTTER_MAIN): $(PLOTTER_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lboost_program_options -L./$(BIN_DIR) -B,-dynamic -lpdcurses

#//////////////////////////////////////////////////////////////////////////////////////
#	Fake targets
#
#//////////////////////////////////////////////////////////////////////////////////////
.PHONY: clean
.PHONY: all
clean:
	rm -f ./*.o
	rm -f ./*.exe
	rm -f $(BIN_DIR)/*.exe
	rm -f $(LIB_DIR)/$(DISTRIBUTION_LIB)
	rm -f $(OBJ_DIR)/*.o
	rm -f $(OBJ_DIR)/$(GENERATOR_DIR)/*.o
	rm -f $(OBJ_DIR)/$(PLOTTER_DIR)/*.o
	rm -f $(OBJ_DIR)/$(STATISTIC_DIR)/*.o
	rm -f $(OBJ_DIR)/$(DISTRIBUTIONS_DIR)/*.o

all: $(GENERATOR_MAIN) $(STATISTIC_MAIN) $(PLOTTER_MAIN)