#include "types.hpp"

void geometry::init_std_curses()
{
    ::initscr();
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_BLUE, COLOR_BLACK);
    init_pair(4, COLOR_CYAN, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_YELLOW, COLOR_BLACK);
    init_pair(7, COLOR_WHITE, COLOR_BLACK);
}

void geometry::end_curses()
{
    ::endwin();
}

geometry::attribute_guard::attribute_guard(const unsigned int attribute_):
    attribute(attribute_)
    {
        attron(attribute);
    }

geometry::attribute_guard::~attribute_guard()
{
    attroff(attribute);
}

geometry::point::point(const unsigned int x_, const unsigned int y_):
    x(x_),
    y(y_)
    {

    }

geometry::point geometry::point::move(const unsigned int dx, const unsigned int dy) const
{
    return point(x + dx, y + dy);
}

geometry::tip::tip(const geometry::char_type left_top_, const geometry::char_type right_bot_):
    left_top(left_top_),
    right_bot(right_bot_)
    {

    }

geometry::border::border(const char_type left_top_, const char_type top_, const char_type right_top_, const char_type right_, const char_type right_bot_, const char_type bot_, const char_type left_bot_, const char_type left_):
    left_top(left_top_),
    top(top_),
    right_top(right_top_),
    right(right_),
    right_bot(right_bot_),
    bot(bot_),
    left_bot(left_bot_),
    left(left_)
    {

    }

