#include "hplotter.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>

int main(int argc, char * argv[])
{
    std::string version("1.0.1");
    std::string OSTREAM_NAME;
    std::string ISTREAM_NAME;
    AMStatHistogram::min_max_type BORDERS;
    AMStatHistogram::hist_type DENSITY;
    AMStatHistogram::hist_type PROBABILITY;
    using namespace boost::program_options;
    try
    {
        try
        {
            options_description general_desc{"General options"};
            general_desc.add_options()
            ("help,h", "Help screen")
            ("version,v", "Version information")
            ("input,I", value< std::string >()->default_value(""), "Input filename option. Default: standard input");

            variables_map vm;
            store(parse_command_line(argc, argv, general_desc), vm);
            notify(vm);

            if(vm.count("help"))
            {
                std::cout << general_desc << "\n";
                return 0;
            }
            if(vm.count("version"))
            {
                std::cout << "Histogram plotter. Version is: " << version << std::endl;
                return 0;
            }
            ISTREAM_NAME = vm["input"].as< std::string >();
        }
        catch(const error & ex)
        {
            std::cerr << "Wrong command line parameters. Use '-h' or '--help' option" << std::endl;
            return 0;
        }
    }
    catch(...)
    {
        std::cerr << "Error: unexpected command line options. Use '-h' or '--help' option" << std::endl;
        return 0;
    }

    std::string source_name;
    if(!ISTREAM_NAME.size())
    {
        std::cout << ">Borders: ";
        std::istream_iterator< double > biit(std::cin);
        BORDERS.first = *biit++;
        BORDERS.second = *biit;
        std::cin.clear();

        std::cout << ">Amount: ";
        std::istream_iterator< int > iiit(std::cin);
        int amount = *iiit;
        std::cin.clear();
        if(amount > 0)
        {
            std::cout << ">Density: ";
            iiit = std::istream_iterator< int >(std::cin);
            for(int i = 0; i < amount - 1; i++)
            {
                DENSITY.push_back(*iiit++);
            }
            DENSITY.push_back(*iiit);
            std::cin.clear();

            int sum = 0;
            std::transform(DENSITY.begin(), DENSITY.end(), std::back_inserter(PROBABILITY), [&sum](int value)
                           {
                                sum += value;
                                return sum;
                           });
        }
        source_name = "STD";
    }
    else
    {
        std::ifstream input(ISTREAM_NAME);
        std::istream_iterator< double > iit(input);
        BORDERS.first = *iit++;
        BORDERS.second = *iit;
        std::vector< unsigned int > columns = std::vector< unsigned int >(std::istream_iterator< unsigned int >(input), std::istream_iterator< unsigned int >());
        unsigned int amount = columns.size();
        std::copy(columns.begin(), columns.begin() + amount/2, std::back_inserter(DENSITY));
        std::copy(columns.begin() + amount/2, columns.end(), std::back_inserter(PROBABILITY));
        source_name = ISTREAM_NAME;
    }

    if(DENSITY.size() == 0)
    {
        std::cerr << "Error: there is no any column in histogram" << std::endl;
        return 0;
    }

    bool is_negative_dens = std::any_of(DENSITY.begin(), DENSITY.end(), [](int value)
    {
        return value < 0;
    });

    bool is_negative_prob = std::any_of(DENSITY.begin(), DENSITY.end(), [](int value)
    {
        return value < 0;
    });

    if(is_negative_dens || is_negative_prob)
    {
        std::cerr << "Error: negative column value" << std::endl;
        return 0;
    }

    AMStatHistogram hist(BORDERS, DENSITY, PROBABILITY);
    hist.draw(geometry::origin, source_name);
}
