#ifndef HPLOTTER_H
#define HPLOTTER_H
#include "geometry.hpp"
#include "types.hpp"
#include <utility>
#include <vector>
#include <string>
class AMStatHistogram
{
    public:
        typedef std::pair< double, double > min_max_type;
        typedef std::vector< int > hist_type;
        AMStatHistogram(const min_max_type &, const hist_type &, const hist_type &);
        void draw(const geometry::point &, const std::string &);
    private:
        AMStatHistogram();
        geometry::point plot_hist(const geometry::point &, const hist_type &, const std::string &, const std::string &);

        min_max_type min_max_borders;
        hist_type density;
        hist_type probability;
};
#endif // HPLOTTER_H
