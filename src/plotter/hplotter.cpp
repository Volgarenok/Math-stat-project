#include "hplotter.h"
#include <algorithm>
#include <sstream>

namespace
{
    std::string dtostr(const double value)
    {
        std::ostringstream strs;
        strs << value;
        return strs.str();
    }
    const std::string max_l = "Max";
    const std::string min_l = "Min";
    const std::string range_l = "Range";
    const std::string scale_l = "Scale";
    const std::string info = "Information";
    const std::string probability_l = "Probability";
    const std::string density_l = "Density";
}

AMStatHistogram::AMStatHistogram(const min_max_type & min_max, const hist_type & dens, const hist_type & prob):
    min_max_borders(min_max),
    density(dens),
    probability(prob)
    {
    }

geometry::point AMStatHistogram::plot_hist(const geometry::point & origin, const hist_type & sequence, const std::string & name, const std::string & gen_name)
{
    unsigned int column_amount = sequence.size();
    unsigned int max_dot = *std::max_element(sequence.begin(), sequence.end());
    geometry::point sub_origin(origin.move(1, 1));
    geometry::point info_origin(sub_origin.move(max_dot + 4, 0));

    geometry::named_rectangle(gen_name.c_str(), sub_origin, sub_origin.move(max_dot + 2, 2*column_amount + 2), geometry::borders::twin_square, COLOR_PAIR(7) | A_BOLD);
    geometry::snippet_v(sub_origin.move(1, 1), sub_origin.move(1, 2*column_amount + 1), geometry::tips::single_v, geometry::chars::single_cross, COLOR_PAIR(7) | A_BOLD);
    for(unsigned int i = 0; i < column_amount; i++)
    {
        if(sequence[i] != 0)
        {
            geometry::line_h(sub_origin.move(2, 2*(i + 1)), sub_origin.move(2 + sequence[i] - 1, 2*(i + 1)), geometry::chars::star, COLOR_PAIR(7) | A_BOLD);
        }
    }

    std::string left_border = dtostr(min_max_borders.first);
    std::string right_border = dtostr(min_max_borders.second);
    std::string range = dtostr((min_max_borders.second - min_max_borders.first)/column_amount);
    std::string scale = "* = 1";

    std::vector< size_t > lengths(5);
    lengths[0] = left_border.size();
    lengths[1] = right_border.size();
    lengths[2] = range.size();
    lengths[3] = scale.size();
    lengths[4] = info.size();

    size_t max_length_info = *std::max_element(lengths.begin(), lengths.end());

    geometry::named_rectangle(info.c_str(), info_origin, info_origin.move(2 + max_length_info, 1 + 3*(lengths.size() - 1)), geometry::borders::twin_square, COLOR_PAIR(7) | A_BOLD);

    geometry::named_label(min_l.c_str(), left_border.c_str(), info_origin.move(1, 1), info_origin.move(1 + max_length_info, 3), geometry::borders::single_square, COLOR_PAIR(7) | A_BOLD);
    geometry::named_label(max_l.c_str(), right_border.c_str(), info_origin.move(1, 4), info_origin.move(1 + max_length_info, 6), geometry::borders::single_square, COLOR_PAIR(7) | A_BOLD);
    geometry::named_label(range_l.c_str(), range.c_str(), info_origin.move(1, 7), info_origin.move(1 + max_length_info, 9), geometry::borders::single_square, COLOR_PAIR(7) | A_BOLD);
    geometry::named_label(scale_l.c_str(), scale.c_str(), info_origin.move(1, 10), info_origin.move(1 + max_length_info, 12), geometry::borders::single_square, COLOR_PAIR(7) | A_BOLD);

    unsigned int first = geometry::count_width(sub_origin, info_origin.move(2 + max_length_info, 1 + 3*(lengths.size() - 1)));
    unsigned int second = std::max(geometry::count_height(sub_origin, info_origin.move(2 + max_length_info, 1 + 3*(lengths.size() - 1))), geometry::count_height(sub_origin, sub_origin.move(1, 2*column_amount + 1)));

    geometry::named_rectangle(name.c_str(), origin, origin.move(first + 2, second + 2), geometry::borders::single_square, COLOR_PAIR(7) | A_BOLD);
    return geometry::point(0, second + 3);
}

void AMStatHistogram::draw(const geometry::point & origin, const std::string & gen_name)
{
    geometry::init_std_curses();
    geometry::point new_origin = plot_hist(origin, density, gen_name, density_l);
    plot_hist(new_origin, probability, gen_name, probability_l);
    getch();
    geometry::end_curses();
}
