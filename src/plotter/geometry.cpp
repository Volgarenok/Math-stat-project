#include "geometry.hpp"
#include <cstring>
void geometry::print(const std::string & str, const unsigned int x, const unsigned int y)
{
    mvprintw(y, x, str.c_str());
}

const geometry::point & geometry::left_point(const point & pt_1, const point & pt_2)
{
    if(pt_1.x >= pt_2.x)
    {
        return pt_2;
    }
    else
    {
        return pt_1;
    }
}

const geometry::point & geometry::right_point(const point & pt_1, const point & pt_2)
{
    if(pt_1.x >= pt_2.x)
    {
        return pt_1;
    }
    else
    {
        return pt_2;
    }
}

const geometry::point & geometry::top_point(const point & pt_1, const point & pt_2)
{
    if(pt_1.y >= pt_2.y)
    {
        return pt_2;
    }
    else
    {
        return pt_1;
    }
}

const geometry::point & geometry::bot_point(const point & pt_1, const point & pt_2)
{
    if(pt_1.y >= pt_2.y)
    {
        return pt_1;
    }
    else
    {
        return pt_2;
    }
}

unsigned int geometry::count_width(const point & pt_1, const point & pt_2)
{
    const point left = left_point(pt_1, pt_2);
    const point right = right_point(pt_1, pt_2);
    return right.x - left.x + 1;
}

unsigned int geometry::count_height(const point & pt_1, const point & pt_2)
{
    const point bot = bot_point(pt_1, pt_2);
    const point top = top_point(pt_1, pt_2);
    return bot.y - top.y + 1;
}

void geometry::line_h(const point & pt_1, const point & pt_2, const char_type ch, const unsigned int attribute)
{
    attribute_guard guard(attribute);
    if(pt_1.x == pt_2.x)
    {
        string_type element(1, ch);
        mvprintw(pt_1.y, pt_1.x, element.c_str());
        return;
    }

    const point left = left_point(pt_1, pt_2);
    const unsigned int size = count_width(pt_1, pt_2);
    string_type line(size, ch);

    mvprintw(left.y, left.x, line.c_str());
}

void geometry::line_v(const point & pt_1, const point & pt_2, const char_type ch, const unsigned int attribute)
{
    attribute_guard guard(attribute);
    if(pt_1.y == pt_2.y)
    {
        string_type element(1, ch);
        mvprintw(pt_1.y, pt_1.x, element.c_str());
        return;
    }

    const point top = top_point(pt_1, pt_2);
    const unsigned int size = count_height(pt_1, pt_2);

    string_type element(1, ch);
    for(unsigned int i = 0; i < size; i++)
    {
        mvprintw(top.y + i, top.x, element.c_str());
    }
}

void geometry::named_line_h(const char_type * name, const point & pt_1, const point & pt_2, const char_type ch, const unsigned int attribute)
{
    line_h(pt_1, pt_2, ch, attribute);
    const string_type name_line(name);
    const unsigned int size = count_width(pt_1, pt_2);
    const point left = left_point(pt_1, pt_2);

    if(size <= 2)
    {
        return;
    }

    if(name_line.length() + 2 > size)
    {
        attribute_guard guard(attribute);
        string_type out(size - 2, ch);
        out.replace(1, size - 2, name_line, 0, size - 2);
        mvprintw(left.y, left.x, out.c_str());
    }
    else
    {
        attribute_guard guard(attribute);
        mvprintw(left.y, left.x + 2, name_line.c_str());
    }
}

void geometry::snippet_h(const point & pt_1, const point & pt_2, const tip & tp, const char ch, const unsigned int attribute)
{
    const point left = left_point(pt_1, pt_2);
    const point right = right_point(pt_1, pt_2);
    line_h(left.move(1, 0), right.move(-1, 0), ch, attribute);
    line_h(left, left, tp.left_top, attribute);
    line_h(right, right, tp.right_bot, attribute);
}

void geometry::snippet_v(const point & pt_1, const point & pt_2, const tip & tp, const char_type ch, const unsigned int attribute)
{
    const point top = top_point(pt_1, pt_2);
    const point bot = bot_point(pt_1, pt_2);
    line_v(top, bot, ch, attribute);
    line_v(top, top, tp.left_top, attribute);
    line_v(top.move(0, count_height(pt_1, pt_2) - 1), bot, tp.right_bot, attribute);
}

void geometry::rectangle(const point & left_top, const point & right_bot, const border & br, const unsigned int attribute)
{
    if((left_top.x > right_bot.x) || (left_top.y > right_bot.y))
    {
        return;
    }

    if(count_height(left_top, right_bot) < 2 || count_width(left_top, right_bot) < 2)
    {
        return;
    }

    line_h(left_top, right_bot, br.top, attribute);
    line_h(left_top.move(0, count_height(left_top, right_bot) - 1), right_bot, br.bot, attribute);
    snippet_v(left_top, right_bot, tip(br.left_top, br.left_bot), br.left, attribute);
    snippet_v(left_top.move(count_width(left_top, right_bot) - 1, 0), right_bot, tip(br.right_top, br.right_bot), br.right, attribute);
}

void geometry::named_rectangle(const char_type * name, const point & left_top, const point & right_bot, const border & br, const unsigned int attribute)
{
    if((left_top.x > right_bot.x) || (left_top.y > right_bot.y))
    {
        return;
    }

    if(count_height(left_top, right_bot) < 2 || count_width(left_top, right_bot) < 2)
    {
        return;
    }

    named_line_h(name, left_top, right_bot, br.top, attribute);
    line_h(left_top.move(0, count_height(left_top, right_bot) - 1), right_bot, br.bot, attribute);
    snippet_v(left_top, right_bot, tip(br.left_top, br.left_bot), br.left, attribute);
    snippet_v(left_top.move(count_width(left_top, right_bot) - 1, 0), right_bot, tip(br.right_top, br.right_bot), br.right, attribute);
}

void geometry::label(const char_type * label, const point & left_top, const point & right_bot, const border & br, const unsigned int attribute)
{
    const unsigned int width = count_width(left_top, right_bot);
    const unsigned int height = count_height(left_top, right_bot);
    if((left_top.x > right_bot.x) || (left_top.y > right_bot.y))
    {
        return;
    }

    if(height < 3 || width < 3)
    {
        return;
    }

    rectangle(left_top, right_bot, br, attribute);

    string_type label_line(label);
    if(label_line.length() < width - 2)
    {
        attribute_guard guard(attribute);
        mvprintw(left_top.y + 1, left_top.x + 1, label_line.c_str());
    }
    else
    {
        attribute_guard guard(attribute);
        string_type out(width - 2, ' ');
        out.replace(0, width - 2, label_line, 0, width - 2);
        mvprintw(left_top.y + 1, left_top.x + 1, out.c_str());
    }
}

void geometry::named_label(const char_type * name, const char_type * label, const point & left_top, const point & right_bot, const border & br, const unsigned int attribute)
{
    const unsigned int width = count_width(left_top, right_bot);
    const unsigned int height = count_height(left_top, right_bot);
    if((left_top.x > right_bot.x) || (left_top.y > right_bot.y))
    {
        return;
    }

    if(height < 3 || width < 3)
    {
        return;
    }

    named_rectangle(name, left_top, right_bot, br, attribute);

    string_type label_line(label);
    if(label_line.length() < width - 2)
    {
        attribute_guard guard(attribute);
        mvprintw(left_top.y + 1, left_top.x + 1, label_line.c_str());
    }
    else
    {
        attribute_guard guard(attribute);
        string_type out(width - 2, ' ');
        out.replace(0, width - 2, label_line, 0, width - 2);
        mvprintw(left_top.y + 1, left_top.x + 1, out.c_str());
    }
}


