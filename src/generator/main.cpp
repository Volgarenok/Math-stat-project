#include <boost/program_options.hpp>
#include <random>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <cstdlib>

class normal_builder
{
    public:
        typedef double result_type;
        explicit normal_builder(const double m_wait, const double sigma):
            build_distribution(m_wait, sigma),
            generator()
            {
            }

        double operator()(void)
        {
            //double returnable = build_distribution.get();
            double returnable = build_distribution(generator);
            return returnable;
        }

    private:
        std::normal_distribution< double > build_distribution;
        std::default_random_engine generator;
};

int main(int argc, char * argv[])
{
    std::string version("1.0.2");
    unsigned int SEQUENCE_LENGTH = 0;
    double SIGMA = 1.0d;
    double MATH_WAIT = 0.0d;
    std::string OSTREAM_NAME;

    using namespace boost::program_options;
    try
    {
        options_description desc{"Options"};
        desc.add_options()
        ("help,h", "Help screen")
        ("version,v","Version information")
        ("sigma,s", value< double >()->default_value(1.0d),"Sigma normal distribution (default: 1.0)")
        ("math_wait,m", value< double >()->default_value(0.0d), "Math wait normal distribution (default: 0.0)")
        ("length,l", value< unsigned int >(), "Sequence length parameter")
        ("output,O", value< std::string >()->default_value(""), "Output file name (default: standard output)");

        variables_map vm;
        store(parse_command_line(argc, argv, desc), vm);
        notify(vm);

        if(vm.count("help"))
        {
            std::cout << desc << "\n";
            return 0;
        }

        if(vm.count("version"))
        {
            std::cout << "Sequence number generator. Version " << version << std::endl;
            return 0;
        }

        if(vm.count("length"))
        {
            SEQUENCE_LENGTH = vm["length"].as< unsigned int >();
        }
        else
        {
            std::cout << ">Length: ";
            std::cin >> SEQUENCE_LENGTH;
        }

        SIGMA = vm["sigma"].as< double >();
        MATH_WAIT = vm["math_wait"].as< double >();
        OSTREAM_NAME = vm["output"].as< std::string >();
    }
    catch(const error & ex)
    {
        std::cerr << "Unexpected command line format. Use '-h' or '--help' option" << std::endl;
        return 0;
    }
    srand(time(0));
    std::vector< double > sequence(SEQUENCE_LENGTH);
    std::generate(sequence.begin(), sequence.end(), normal_builder(MATH_WAIT, SIGMA));

    if(OSTREAM_NAME.size())
    {
        std::ofstream output(OSTREAM_NAME.c_str());
        std::copy(sequence.begin(), sequence.end(), std::ostream_iterator< double >(output, " "));
    }
    else
    {
        std::copy(sequence.begin(), sequence.end(), std::ostream_iterator< double >(std::cout, " "));
    }
}
