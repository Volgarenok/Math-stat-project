#include "normal_distribution.h"
#include "uniform_distribution.h"
#include "../io/io_state_guard.h"
#include <cstdlib>

Normal_distribution::Normal_distribution(const double math_wait_, const double sigma_):
    math_wait_amount(math_wait_),
    sigma_amount(sigma_)
    {

    }

double Normal_distribution::sigma() const
{
    return sigma_amount;
}

double Normal_distribution::math_wait() const
{
    return math_wait_amount;
}

double Normal_distribution::get() const
{
    double r_sum = 0.0d;

    Uniform_distribution ud(0.0d, 1.0d);
    for(unsigned int i = 0; i < 12; i++)
    {
        r_sum += ud.get();
    }

    double returnable = math_wait_amount + sigma_amount*(r_sum - 6.0d);
    return returnable;
}

std::ostream & operator<< (std::ostream & out, const Normal_distribution & distribution)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
        out << "(:type " << "normal ";
        out << ":math_wait " << distribution.math_wait() << " ";
        out << ":sigma " << distribution.sigma() << ")";
    }
    return out;
}
