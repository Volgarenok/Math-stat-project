#ifndef NORMAL_DISTRIBUTION_H
#define NORMAL_DISTRIBUTION_H
#include <ostream>
class Normal_distribution
{
    public:
        Normal_distribution(const double math_wait_ = 0.0d, const double sigma_ = 1.0d);
        double get() const;
        double math_wait() const;
        double sigma() const;

    private:
        double math_wait_amount;
        double sigma_amount;
};

std::ostream & operator<< (std::ostream & out, const Normal_distribution & distribution);
#endif // NORMAL_DISTRIBUTION_H
