#include "stat_unit.h"
#include <boost/program_options.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>

int main(int argc, char * argv[])
{
    std::string version("1.0.1");
    std::string OSTREAM_NAME;
    std::string ISTREAM_NAME;
    std::vector< double > SEQUENCE;

    int GROUP_AMOUNT = 0;
    int SAMPLE_AMOUNT;

    double TRUST_PROBABILITY = 0.95d;
    double PART = 0.2d;

    using namespace boost::program_options;
    try
    {
        try
        {
            options_description general_desc{"General options"};
            general_desc.add_options()
            ("help,h", "Help screen")
            ("version,v","Version information")
            ("output,O", value< std::string >()->default_value(""), "Output histogram filename")
            ("input,I", value< std::string >()->default_value(""), "Input filename option. Default: standard input")
            ("trust,t", value< double >(), "Trust probability (default: 0.95)")
            ("part,p", value< double >(), "Part of sequence for sub sequences (default: 20%)")
            ("columns,c", value< int >(), "Amount of columns for grouping");

            variables_map vm;
            store(parse_command_line(argc, argv, general_desc), vm);
            notify(vm);

            if(vm.count("help"))
            {
                std::cout << general_desc << "\n";
                return 0;
            }
            if(vm.count("version"))
            {
                std::cout << "Sequence statistic unit. Version is: " << version << std::endl;
                return 0;
            }
            OSTREAM_NAME = vm["output"].as< std::string >();
            ISTREAM_NAME = vm["input"].as< std::string >();

            if(vm.count("trust"))
            {
                TRUST_PROBABILITY = vm["trust"].as< double >();
            }
            if(vm.count("part"))
            {
                PART = vm["part"].as< double >();
            }

            if(vm.count("columns"))
            {
                GROUP_AMOUNT = vm["columns"].as< int >();
            }
            else
            {
                std::cout << ">Column amount: ";
                std::istream_iterator< int > iiit(std::cin);
                int amount = *iiit;
                if(amount < 1)
                {
                    std::cerr << "Error: amount of columns must be positive" << std::endl;
                    return 0;
                }
                GROUP_AMOUNT = amount;
            }
        }
        catch(const error & ex)
        {
            std::cerr << "Wrong command line parameters. Use '-h' or '--help' option" << std::endl;
            return 0;
        }
    }
    catch(...)
    {
        std::cerr << "Error: unexpected command line options. Use '-h' or '--help' option" << std::endl;
        return 0;
    }

    if(!ISTREAM_NAME.size())
    {
        std::cout << ">Sequence amount: ";
        std::istream_iterator< int > iiit(std::cin);
        int amount = *iiit;
        std::cin.clear();
        if(amount > 0)
        {
            std::cout << ">Sequence: ";
            std::istream_iterator< double > diit(std::cin);
            for(int i = 0; i < amount - 1; i++)
            {
                SEQUENCE.push_back(*diit++);
            }
            SEQUENCE.push_back(*diit);
            std::cin.clear();
        }
    }
    else
    {
        std::ifstream input(ISTREAM_NAME);
        try
        {
            SEQUENCE = std::vector< double >(std::istream_iterator< double >(input), std::istream_iterator< double >());
        }
        catch(...)
        {
            std::cerr << "Error: file does not exist\n" << std::endl;
            return 0;
        }
    }

    SAMPLE_AMOUNT = PART*SEQUENCE.size();

    stat_unit::SUnit stat_unit(SEQUENCE);
    std::cout << ">Sequence size: " << SEQUENCE.size() << std::endl;
    std::cout << ">Sample size: " << SAMPLE_AMOUNT << std::endl;
    std::cout << ">Average: " << stat_unit.average() << std::endl;
    std::cout << ">Dispersion: " << stat_unit.dispersion() << std::endl;
    std::cout << ">P-Dispersion: " << stat_unit.pdispersion() << std::endl;
    std::cout << ">Median: " << stat_unit.median() << std::endl;
    std::cout << ">Density group: " << stat_unit.density(GROUP_AMOUNT) << std::endl;
    std::cout << ">Probability group: " << stat_unit.probability(GROUP_AMOUNT) << std::endl;

    stat_unit::SUnit::histogram dens = stat_unit.density(GROUP_AMOUNT);
    stat_unit::SUnit::histogram prob = stat_unit.probability(GROUP_AMOUNT);

    stat_unit::SUnit::group_table group = stat_unit.density_to_table(dens);
    std::cout << ">Group table:\n" << group;
    std::cout << ">Average group: " << stat_unit.average(group) << std::endl;
    std::cout << ">Dispersion(group): " << stat_unit.dispersion(group) << std::endl;
    std::cout << ">P-dispersion(group): " << stat_unit.pdispersion(group) << std::endl;
    std::cout << ">Shepard: " << stat_unit.Shepard(dens) << std::endl;
    std::cout << ">Central Moment(3): " << stat_unit.pmoment(group, stat_unit.average(group), 3) << std::endl;
    std::cout << ">Central Moment(4): " << stat_unit.pmoment(group, stat_unit.average(group), 4) << std::endl;
    std::cout << ">Excess(Kurtosis): " << stat_unit.kurtosis(group) - 3 << std::endl;
    std::cout << ">Skewness: " << stat_unit.skewness(group) << std::endl;
    std::cout << ">Dispersion(sample): " << stat_unit.dispersion(SAMPLE_AMOUNT) << std::endl;
    std::cout << ">P-dispersion(sample): " << stat_unit.pdispersion(SAMPLE_AMOUNT) << std::endl;

    std::pair< double, double > sinterval = stat_unit.strust(SAMPLE_AMOUNT, TRUST_PROBABILITY);
    std::cout << ">Trust interval(Student): " << sinterval.first << " " << sinterval.second << std::endl;
    std::pair< double, double > chiinterval = stat_unit.chitrust(SAMPLE_AMOUNT, TRUST_PROBABILITY);
    std::cout << ">Dispersion interval(Chi-Square): " << chiinterval.first << " " << chiinterval.second << std::endl;

    std::tuple< unsigned int, unsigned int, unsigned int > wilcoxon_test = stat_unit.wilcoxon(SAMPLE_AMOUNT);
    std::cout << ">Wilcoxon: " << std::get< 0 >(wilcoxon_test) << " " << std::get< 1 >(wilcoxon_test) << " " << std::get< 2 >(wilcoxon_test) << std::endl;

    std::tuple< double, double, double > uwilcoxon = stat_unit.wilcoxon(SAMPLE_AMOUNT, TRUST_PROBABILITY);
    std::cout << ">Wilcoxon-Uniform test: " << std::get< 0 >(uwilcoxon) << " " << std::get< 1 >(uwilcoxon) << " " << std::get< 2 >(uwilcoxon) << std::endl;
    std::cout << ">Signed-Uniform test: " << stat_unit.signedtest(SAMPLE_AMOUNT) << std::endl;

    std::tuple< double, double, double > pirson_10 = stat_unit.chi_pirson(dens, 0.90d);
    std::tuple< double, double, double > pirson_5 = stat_unit.chi_pirson(dens, 0.95d);
    std::tuple< double, double, double > pirson_1 = stat_unit.chi_pirson(dens, 0.99d);
    std::cout << ">Chi-Square Normal-test(Pirson) 10% : " << std::get< 1 >(pirson_10) << " " << std::get< 2 >(pirson_10) << std::endl;
    std::cout << ">Chi-Square Normal-test(Pirson) 5% : " << std::get< 1 >(pirson_5) << " " << std::get< 2 >(pirson_5) << std::endl;
    std::cout << ">Chi-Square Normal-test(Pirson) 1% : " << std::get< 1 >(pirson_1) << " " << std::get< 2 >(pirson_1) << std::endl;

    if(OSTREAM_NAME.size())
    {
        std::fstream output;
        output.open(OSTREAM_NAME, std::fstream::out);
        output << std::get< 0 >(dens) << " " << std::get< 1 >(dens) << std::endl;
        std::copy(std::get< 2 >(dens).begin(), std::get< 2 >(dens).end(), std::ostream_iterator< double >(output, " "));
        output << "\n";
        std::copy(std::get< 2 >(prob).begin(), std::get< 2 >(prob).end(), std::ostream_iterator< double >(output, " "));
    }
}
