#include "stat_unit.h"
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/students_t.hpp>
#include <boost/math/distributions/normal.hpp>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <cmath>
namespace
{
    class disperser
    {
        public:
            disperser(unsigned int size):
                item_squares(size),
                item_sum(0.0d),
                count(0)
            {
            }

            void operator()(const double value)
            {
                item_squares[count++] = value*value;
                item_sum += value;
            }

            double get_d() const
            {
                calculate functor = std::for_each(item_squares.begin(), item_squares.end(), calculate(item_sum, count));
                return functor.get_d();
            }

            double get_pd() const
            {
                calculate functor = std::for_each(item_squares.begin(), item_squares.end(), calculate(item_sum, count));
                return functor.get_pd();
            }

        private:
            class calculate
            {
                public:
                    calculate(const double sum_, const unsigned int size_):
                        dispersion(0.0d),
                        mean_square(sum_/size_ * sum_/size_),
                        size(size_)
                    {
                    }
                    void operator()(const double item_square)
                    {
                        dispersion += item_square - mean_square;
                    }

                    double get_d() const
                    {
                        return dispersion/(size);
                    }

                    double get_pd() const
                    {
                        return dispersion/(size - 1);
                    }

                private:
                    double dispersion;
                    double mean_square;
                    unsigned int size;
            };

            std::vector< double > item_squares;
            double item_sum;
            unsigned int count;
    };
}

std::ostream & operator<< (std::ostream & out, const stat_unit::SUnit::histogram & hist)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
        out << "(:min " << std::get< 0 >(hist) << " ";
        out << ":max " << std::get< 1 >(hist) << " ";
        out << ":range " << (std::get< 1 >(hist) - std::get< 0 >(hist))/std::get< 2 >(hist).size() << " ";
        out << ":hist ";
        std::copy(std::get< 2 >(hist).begin(), std::get< 2 >(hist).end(), std::ostream_iterator< unsigned int >(out, " "));
        out << ")";
    }
    return out;
}

std::ostream & operator<< (std::ostream & out, const stat_unit::SUnit::table_string & tstring)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        out << " " << std::setw(3) << std::get< 0 >(tstring) << " ";
        out << " " << std::setw(9) << std::get< 1 >(tstring) << " ";
        out << " " << std::setw(6) << std::get< 2 >(tstring) << " ";
    }
    return out;
}

std::ostream & operator<< (std::ostream & out, const stat_unit::SUnit::group_table & table)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        out << " Num  MeanRange  Amount \n";
        std::for_each(table.begin(), table.end(), [&out](const stat_unit::SUnit::table_string & tstring)
                      {
                          out << tstring << "\n";
                      });
    }
    return out;
}

stat_unit::SUnit::SUnit(const std::vector< double > & rhs):
    seq(rhs)
    {
    }

double stat_unit::SUnit::average() const
{
    double sum = std::accumulate(seq.begin(), seq.end(), 0.0d);
    return sum/seq.size();
}

double stat_unit::SUnit::dispersion() const
{
    disperser functor = std::for_each(seq.begin(), seq.end(), disperser(seq.size()));
    return functor.get_d();
}

double stat_unit::SUnit::pdispersion() const
{
    disperser functor = std::for_each(seq.begin(), seq.end(), disperser(seq.size()));
    return functor.get_pd();
}

double stat_unit::SUnit::median() const
{
    std::vector< double > copied = seq;
    std::sort(copied.begin(), copied.end());
    unsigned int mod = copied.size() % 2;
    unsigned int div = copied.size() / 2;
    if(mod == 0)
    {
        return (copied[div - 1] + copied[div])/2;
    }
    else
    {
        return copied[div];
    }
}

stat_unit::SUnit::histogram stat_unit::SUnit::density(const unsigned int columns) const
{
    double maximum = *std::max_element(seq.begin(), seq.end());
    double minimum = *std::min_element(seq.begin(), seq.end());
    double range = (maximum - minimum)/columns;
    std::vector< unsigned int > returnable(columns);
    std::for_each(seq.begin(), seq.end(), [&returnable, minimum, range](const double & value) mutable
                  {
                      for(unsigned int i = 0; i < returnable.size(); i++)
                      {
                          if(minimum + i*range <= value && value <= minimum + (i + 1)*range)
                          {
                              returnable[i]++;
                          }
                      }
                  });
    unsigned int res = std::accumulate(returnable.begin(), returnable.end(), 0u);
    assert(res == seq.size());
    return std::make_tuple(minimum, maximum, returnable);
}

stat_unit::SUnit::histogram stat_unit::SUnit::probability(const unsigned int columns) const
{
    histogram density_hist = density(columns);
    unsigned int sum = 0;
    std::for_each(std::get< 2 >(density_hist).begin(), std::get< 2 >(density_hist).end(), [&sum](unsigned int & value)
                  {
                      unsigned int old_value = value;
                      value += sum;
                      sum += old_value;
                  });
    return density_hist;
}

double stat_unit::SUnit::average(const group_table & table) const
{
    table_string returnable = std::accumulate(table.begin(), table.end(), std::make_tuple(0u, 0.0d, 0u), [](const group_table::value_type & lhs, const group_table::value_type & rhs)
                                        {
                                            return std::make_tuple(0u, std::get< 1 >(lhs) + std::get< 1 >(rhs)* std::get< 2 >(rhs),  std::get< 2 >(lhs) +  std::get< 2 >(rhs));
                                        });
    return std::get< 1 >(returnable)/std::get< 2 >(returnable);
}

stat_unit::SUnit::group_table stat_unit::SUnit::density_to_table(const histogram & probability) const
{
    group_table returnable(std::get< 2 >(probability).size());
    double minimun = std::get< 0 >(probability);
    double maximum = std::get< 1 >(probability);
    double range = (maximum - minimun)/returnable.size();
    unsigned int iter = 0;
    std::transform(std::get< 2 >(probability).begin(), std::get< 2 >(probability).end(), returnable.begin(), [&minimun, &range, &iter](const unsigned int value)
                   {
                       double mean_in_range = minimun + iter*range + range/2;
                       return std::make_tuple(iter++, mean_in_range, value);
                   });
    return returnable;
}

std::vector< double > stat_unit::SUnit::table_to_sequence(const group_table & table) const
{
    std::vector< double > returnable;
    std::for_each(table.begin(), table.end(), [&returnable](const table_string & tstring)
                  {
                      for(unsigned int i = 0; i < std::get< 2 >(tstring); i++)
                      {
                          returnable.push_back(std::get< 1 >(tstring));
                      }
                  });
    return returnable;
}

double stat_unit::SUnit::dispersion(const group_table & table) const
{
    std::vector< double > group_sequence = table_to_sequence(table);
    disperser functor = std::for_each(group_sequence.begin(), group_sequence.end(), disperser(group_sequence.size()));
    return functor.get_d();
}

double stat_unit::SUnit::pdispersion(const group_table & table) const
{
    std::vector< double > group_sequence = table_to_sequence(table);
    disperser functor = std::for_each(group_sequence.begin(), group_sequence.end(), disperser(group_sequence.size()));
    return functor.get_pd();
}

double stat_unit::SUnit::Shepard(const histogram & dens) const
{
    double range = (std::get< 1 >(dens) - std::get< 0 >(dens))/std::get< 2 >(dens).size();
    return range * range / 12;
}

double stat_unit::SUnit::pmoment(const group_table & table, const double group_aver, const unsigned int number = 1) const
{
    table_string result = std::accumulate(table.begin(), table.end(), std::make_tuple(0u, 0.0d, 0u), [group_aver, number](const table_string & lhs, const table_string & rhs)
                                          {
                                              return std::make_tuple(0u, std::get< 1 >(lhs) + std::pow(std::get< 1 >(rhs) - group_aver, number)*std::get< 2 >(rhs), std::get< 2 >(lhs) + std::get< 2 >(rhs));
                                          });
    return std::get< 1 >(result)/std::get< 2 >(result);
}

double stat_unit::SUnit::kurtosis(const group_table & table) const
{
    double fourth_central_moment = pmoment(table, average(table), 4);
    return fourth_central_moment/(std::pow(dispersion(table), 2));
}

double stat_unit::SUnit::skewness(const group_table & table) const
{
    double third_central_moment = pmoment(table, average(table), 3);
    return third_central_moment/(std::pow(dispersion(table), 1.5d));
}

double stat_unit::SUnit::dispersion(const unsigned int value) const
{
    assert(value <= seq.size());
    assert(value > 0);
    std::vector< double >::const_iterator sample_end = seq.begin();
    std::advance(sample_end, value);
    std::vector< double > sample(seq.begin(), sample_end);
    disperser functor = std::for_each(sample.begin(), sample.end(), disperser(sample.size()));
    return functor.get_d();
}

double stat_unit::SUnit::pdispersion(const unsigned int value) const
{
    assert(value <= seq.size());
    assert(value > 1);
    std::vector< double >::const_iterator sample_end = seq.begin();
    std::advance(sample_end, value);
    std::vector< double > sample(seq.begin(), sample_end);
    disperser functor = std::for_each(sample.begin(), sample.end(), disperser(sample.size()));
    return functor.get_pd();
}

std::pair< double, double > stat_unit::SUnit::strust(const unsigned int value, const double p) const
{
    using namespace boost::math;
    assert(value <= seq.size());
    assert(value > 1);
    double pdisp = pdispersion(value);
    std::vector< double >::const_iterator sample_end = seq.begin();
    std::advance(sample_end, value);
    double sum = std::accumulate(seq.begin(), sample_end, 0.0d);
    students_t_distribution<> distr(value - 1);
    double student_coefficient = quantile(distr, (1 + p)/2);
    return std::make_pair(sum/value - student_coefficient*std::sqrt(pdisp/value), sum/value + student_coefficient*std::sqrt(pdisp/value));
}

std::pair< double, double > stat_unit::SUnit::chitrust(const unsigned int value, const double p) const
{
    using namespace boost::math;
    assert(value <= seq.size());
    assert(value > 1);
    double disp = pdispersion(value);
    chi_squared_distribution<> chi_distr(value - 1);
    double chi_low = quantile(chi_distr, p);
    double chi_high = quantile(complement(chi_distr, p));
    return std::make_pair((value - 1)*disp/chi_low, (value - 1)*disp/chi_high);
}

std::tuple< unsigned int, unsigned int, unsigned int > stat_unit::SUnit::wilcoxon(const unsigned int sample) const
{
    assert(2*sample <= seq.size());
    assert(sample > 0);
    unsigned int same = 0u;
    unsigned int less = 0u;
    unsigned int more = 0u;
    std::vector< double > left = std::vector< double >(seq.begin(), seq.begin() + sample);
    std::vector< double > right = std::vector< double >(seq.rbegin(), seq.rbegin() + sample);
    for(unsigned int i = 0; i < sample; i++)
    {
        for(unsigned int j = 0; j < sample; j++)
        {
            if(right[j] < left[i])
            {
                less++;
            }
            else if(right[j] > left[i])
            {
                more++;
            }
            else
            {
                same++;
            }
        }
    }
    return std::make_tuple(less, more, same);
}

std::tuple< double, double, double > stat_unit::SUnit::wilcoxon(const unsigned int sample, double p) const
{
    using namespace boost::math;
    assert(2*sample <= seq.size());
    assert(sample > 0);
    unsigned int same = 0u;
    unsigned int less = 0u;
    unsigned int more = 0u;
    std::vector< double > left = std::vector< double >(seq.begin(), seq.begin() + sample);
    std::vector< double > right = std::vector< double >(seq.rbegin(), seq.rbegin() + sample);
    std::sort(left.begin(), left.end());
    std::sort(right.begin(), right.end());
    std::vector< unsigned int > inversions(sample);
    std::transform(left.begin(), left.end(), inversions.begin(), [&right](const double value)
                   {
                       unsigned int inversion = std::count_if(right.begin(), right.end(), [&value](const double in_value)
                                                              {
                                                                  return in_value < value;
                                                              });
                        return inversion;
                   });
    double sum_inversions = static_cast< double >(std::accumulate(inversions.begin(), inversions.end(), 0u));
    double math_wait = static_cast< double >(sample*sample)/2;
    double disp = static_cast< double >(sample*sample*(2*sample + 1))/12;

    normal_distribution<> normal(math_wait, std::pow(disp, 0.5d));

    double right_border = quantile(normal, p);
    double left_border = quantile(complement(normal, p));
    return std::make_tuple(left_border, sum_inversions, right_border);
}

double stat_unit::SUnit::signedtest(const unsigned int sample) const
{
    assert(2*sample <= seq.size());
    assert(sample > 0);
    unsigned int positive = 0u;
    unsigned int negative = 0u;
    unsigned int zeros = 0u;
    std::vector< double > left = std::vector< double >(seq.begin(), seq.begin() + sample);
    std::vector< double > right = std::vector< double >(seq.rbegin(), seq.rbegin() + sample);
    for(unsigned int i = 0; i < sample; i++)
    {
        if(right[i] - left[i] > 0)
        {
            positive++;
        }
        else if(right[i] - left[i] < 0)
        {
            negative++;
        }
        else
        {
            zeros++;
        }
    }
    return static_cast< double >(std::min(positive, negative))/sample;
}

std::tuple< double, double, double > stat_unit::SUnit::chi_pirson(const histogram & dens, const double prob) const
{
    using namespace boost::math;
    double aver = average();
    double sigma = std::sqrt(pdispersion());
    normal_distribution<> normal(aver, sigma);
    unsigned int value = std::get< 2 >(dens).size() - 1;
    double range = (std::get< 1 >(dens) - std::get< 0 >(dens))/std::get< 2 >(dens).size();
    double minimum = std::get< 0 >(dens);
    double chi_square_value = 0.0d;
    double sum = static_cast< double >(std::accumulate(std::get< 2 >(dens).begin(), std::get< 2 >(dens).end(), 0u));
    for(unsigned int i = 0; i < std::get< 2 >(dens).size(); i++)
    {
        double delta_p = cdf(normal, minimum + range*(i+1)) - cdf(normal, minimum + range*i);
        double colum_value = static_cast< double >(std::get< 2 >(dens)[i]);
        chi_square_value += std::pow(colum_value - sum*delta_p, 2)/(sum*delta_p);
    }

    chi_squared_distribution<> chi_low_distr(value - 1);
    chi_squared_distribution<> chi_high_distr(value);
    double chi_low = quantile(chi_low_distr, prob);
    double chi_high = quantile(chi_high_distr, prob);
    return std::make_tuple(chi_low, chi_square_value, chi_high);
}


