#ifndef STAT_UNIT_H
#define STAT_UNIT_H
#include "../io/io_state_guard.h"
#include <vector>
#include <tuple>
namespace stat_unit
{
    class SUnit
    {
        public:
            typedef std::tuple< double, double, std::vector< unsigned int > > histogram;
            typedef std::tuple< unsigned int, double, unsigned int > table_string;
            typedef std::vector< table_string > group_table;

            SUnit(const std::vector< double > &);

            double average() const;
            double average(const group_table &) const;

            double dispersion() const;
            double dispersion(const group_table &) const;
            double dispersion(const unsigned int) const;

            double pdispersion() const;
            double pdispersion(const group_table &) const;
            double pdispersion(const unsigned int) const;

            double median() const;
            histogram density(const unsigned int) const;
            histogram probability(const unsigned int) const;
            group_table density_to_table(const histogram &) const;
            double Shepard(const histogram &) const;

            double pmoment(const group_table &, const double, const unsigned int) const;
            double kurtosis(const group_table &) const;
            double skewness(const group_table &) const;

            std::pair< double, double > strust(const unsigned int, const double) const;
            std::pair< double, double > chitrust(const unsigned int, const double) const;

            std::tuple< unsigned int, unsigned int, unsigned int > wilcoxon(const unsigned int) const;
            std::tuple< double, double, double > wilcoxon(const unsigned int, const double) const;
            double signedtest(const unsigned int) const;

            std::tuple< double, double, double > chi_pirson(const histogram &, const double) const;

        private:
            SUnit();
            SUnit(const SUnit &);
            SUnit & operator=(const SUnit &);

            std::vector< double > table_to_sequence(const group_table & table) const;

            const std::vector< double > seq;
    };
}
std::ostream & operator<< (std::ostream & out, const stat_unit::SUnit::histogram & hist);
std::ostream & operator<< (std::ostream & out, const stat_unit::SUnit::table_string & table);
std::ostream & operator<< (std::ostream & out, const stat_unit::SUnit::group_table & table);
#endif // STAT_UNIT_H
